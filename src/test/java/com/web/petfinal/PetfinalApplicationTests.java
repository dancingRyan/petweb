package com.web.petfinal;

import com.web.petfinal.entity.Banner;
import com.web.petfinal.entity.User;
import com.web.petfinal.mapper.AdminMapper;
import com.web.petfinal.mapper.BannerMapper;
import com.web.petfinal.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@SpringBootTest
class PetfinalApplicationTests {
    @Autowired(required = false)
    BannerMapper bannerMapper;
    @Test
    void contextLoads() {

        Banner b = new Banner();
        b.setId(9);
        b.setUrl("15.jpg");

        /*System.out.println(b);*/

        bannerMapper.selectAll();


    }
    @Autowired(required = false)
    UserMapper userMapper;

    @Autowired(required = false)
    AdminMapper adminMapper;

    @Value("${imageDirPath}")
    private String imageDirPath;

    @Test
    void findPath(){
        User user=new User();
        user.setId(7);
        String url = userMapper.findPictureById(user);
        //得到文件的完整路径
        String path = imageDirPath+url;

        System.out.println(path);
    }
    /*@Test
    void add(){
        User user =new User();
        user.setId(15);
        user.setNickname("艹");

        System.out.println(user);
    }
    @Test
    void selectId(){
        User user =new User();
        User a=userMapper.selectId("闻人月");
        System.out.println(a);

    }*/

    @Test
    void pic(){
        /*String url= userMapper.selectPicture(8);*/

        /*System.out.println("获取到的url:"+url);

        url="../images/user"+String.valueOf(url);

        System.out.println("加新路径拼接后的:"+url);*/
    }

    @Test
    void select(){
        System.out.println(adminMapper.selectTeamShow());
    }




}
