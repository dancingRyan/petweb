package com.web.petfinal.mapper;



import com.web.petfinal.entity.AdoptMessage;
import com.web.petfinal.entity.Banner;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AdoptMessageMapper {

    @Insert("insert into adoptmessage values(null,#{name},#{sex},#{telephone},#{email},#{address},#{remark},#{userid},null)")
    void insertMessage(AdoptMessage adoptMessage);
}
