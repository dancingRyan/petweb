package com.web.petfinal.mapper;



import com.web.petfinal.entity.PetShow;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PetShowMapper {

    @Select("select showpic from petshow limit 0,8")
    List<PetShow> selectShowPic();
}
