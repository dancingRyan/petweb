package com.web.petfinal.mapper;

import com.web.petfinal.entity.Blog;
import com.web.petfinal.entity.Pet;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface PetMapper {

    /*将图片放到pet中*/
    @Update("update pet set  pic=#{pic}  where id=#{id}")
    void updatePicture(Pet pet);
    @Insert("insert into pet values(null,#{title},#{petname},#{petType},#{sex},#{birthday},#{pic},0,#{remark},#{user_id})")
    void insertAll(Pet pet);

 /*查询宠物领养信息*/
    @Select("select id,title,petname,petType,sex,birthday,pic,state,remark,user_id from pet order by id desc limit 0,8")
    List<Pet> selectPetBlog();

    @Select("select id,title,petname,petType,sex,birthday,pic,state,remark from pet where id=13")
    List<Pet> selectPetMessage();
    @Update("update pet set  pic=#{pic}  where user_id=#{user_id}")
    void updatepic(Pet pet);
}
