package com.web.petfinal.mapper;


import com.web.petfinal.entity.Admin;
import com.web.petfinal.entity.Banner;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AdminMapper {

    /*@遍历团队人员信息*/
    @Select("select picture,nickname,sign from admin order by id limit 0,4")
    List<Admin> selectTeamShow();

    /*登录查询*/
    @Select("select id,username,password from admin where username=#{username}")
    Admin adminlogin(String username);

    /*查询全部*/
    @Select("select id,username,password,picture,nickname,sign from admin where username=#{username}")
    Admin SelectAll(String username);


    /*获取管理员全部信息*/
    @Select("select id,username,password,nickname,sex,age,telephone,birthday,Email,address,picture,sign from admin")
    List<Admin> findadmin();

    //新添管理员信息
    @Insert("insert into admin values(null,#{username},#{password},'xiaosan',#{sex},#{age},#{telephone},'2010-12-12',#{Email}," +
            "#{address},null,'123')")
    void createcrmadmin(Admin admin);


    //删除管理员
    @Delete("delete from admin where id=#{id}")
    void deleteadmin(int id);

    /*修改管理员*/
    @Update("update admin set username=#{username},nickname=#{nickname},age=#{age},sex=#{sex}," +
            "telephone=#{telephone},Email=#{Email},address=#{address} where id=#{id}")
    void updateById(Admin admin);


    /*模糊查询*/
    @Select("select * from admin where username like concat('%',#{username},'%')")
    List<Admin> selectLike(String username);
}
