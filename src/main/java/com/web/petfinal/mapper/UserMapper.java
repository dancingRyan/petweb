package com.web.petfinal.mapper;


import com.web.petfinal.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;


@Mapper
public interface UserMapper {
    /*查询用户角色id为3的*/
    @Select("select id,username,password from user where username=#{username}")
    User login(String username);


    /*修改个人用户信息*/
    @Update("update user set username=#{username},nickname=#{nickname},password=#{password},age=#{age},sex=#{sex}," +
            "telephone=#{telephone},Email=#{Email},address=#{address},state=#{state} where username=#{username}")
    void updateByUserName(User user);

    /*根据用户名查询id*/
    @Select("select id from user where username=#{username}")
    User selectId(String username);

    @Insert("insert into user values(null,#{username},#{password},#{nickname},null,#{age},null,null,#{Email},null," +
            "null,#{state},null,null,null)")
    void register(User user);


    @Select("select picture from user where id=#{id}")
    String findPictureById(User user);

    @Update("update user set  picture=#{picture}  where id=#{id}")
    void updatePicture(User user);

    @Select("select id,username,password,picture,nickname,state,sign,request from user where username=#{username}")
    User SelectAll(String username);


    @Select("select picture from user where id=#{id}")
    String selectPicture(Integer id);

    @Select("select nickname from user where id=#{id}")
    String selectNickName(Integer id);

    @Select("select nickname,picture,sign from user where id=#{id}")
    List<User> selectMessage(Integer id);

    @Select("select sign from user where id=#{id}")
    String selectSign(Integer id);

    /*改签名*/
    @Update("update user set  sign=#{sign}  where id=#{id}")
    void updateSign(User user);

    /*查询用户信息*/
    @Select("select id,username,password,sex,age,telephone,birthday,Email,address,picture,state,sign,request from user")
    List<User> finduser();


    //新添用户信息
    @Insert("insert into user values(null,#{username},#{password},'xiaosan',#{sex},#{age},#{telephone},'2010-12-12',#{Email}," +
            "#{address},'1.jpg',#{state},'123','0','2021-07-14')")
    void createcrmclass(User user);

    // 删除用户
    @Delete("delete from user where id=#{id}")
    void deletecrmclass(int id);

    /*修改用户*/
    @Update("update user set username=#{username},nickname=#{nickname},age=#{age},sex=#{sex}," +
            "telephone=#{telephone},Email=#{Email},address=#{address},state=#{state} where id=2")
    void updateById(User user);

    // 7.13模糊查询
    @Select("select * from user where username like concat('%',#{username},'%')")
    List<User> selectLike(String username);


}
