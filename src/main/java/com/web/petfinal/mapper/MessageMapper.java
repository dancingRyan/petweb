package com.web.petfinal.mapper;


import com.web.petfinal.entity.Message;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface MessageMapper {


    @Insert("insert into message values(null,#{name},#{email},#{subject},#{tel},#{message})")
    void insertMessage(Message message);
}
