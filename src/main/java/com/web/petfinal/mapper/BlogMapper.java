package com.web.petfinal.mapper;



import com.web.petfinal.entity.Banner;
import com.web.petfinal.entity.Blog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface BlogMapper {


    @Select("select id,actionTime,peoples,event,title,picture from blog order by actionTime desc limit 0,8")
    List<Blog> selectBlog();
}
