package com.web.petfinal.controller;


import com.web.petfinal.entity.Banner;
import com.web.petfinal.mapper.BannerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BannerController {
    @Autowired(required = false)
    BannerMapper bMapper;

    @RequestMapping("/selectbanner")
    public List<Banner> selectbanner(){
        System.out.println(bMapper);

        return bMapper.selectAll();
    }

}
