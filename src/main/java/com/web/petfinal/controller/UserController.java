package com.web.petfinal.controller;


import com.web.petfinal.entity.User;
import com.web.petfinal.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
public class UserController {
    @Autowired(required = false)
    UserMapper uMapper;

    /*登录判断*/
    @RequestMapping("/login")
    public int login(User user, String rem, HttpServletResponse response,
                     HttpSession session) {
        //查询用户输入的用户名对应的数据库中的信息
        User u = uMapper.login(user.getUsername());

        if (u != null) {//查询到用户名对应的信息
            if (user.getPassword().equals(u.getPassword())) {
                //把登录成功的user对象装进session里面

                u = uMapper.SelectAll(user.getUsername());


             /*   *//*把数据都传进session*//*
                u.setPicture(user.getPicture());
                u.setNickname(user.getNickname());
                u.setState(user.getState());
                u.setSign(user.getSign());
                u.setRequest(user.getRequest());*/

                session.setAttribute("user", u);
                /*判断是否获取到id了*/
                System.out.println("user =" + u + ",rem=" + rem);

                //判断是否需要记住用户名和密码
                if (rem != null) {
                    //创建Cookie对象 把用户名和密码保存进去
                    Cookie c1 = new Cookie("username", user.getUsername());
                    Cookie c2 = new Cookie("password", user.getPassword());
                    //设置Cookie的保存时间 单位秒
                    c1.setMaxAge(60 * 60 * 24 * 30);
                    //通过响应对象response把cookie下发给客户端
                    response.addCookie(c1);
                    response.addCookie(c2);
                }
                return 1;//代表登录成功
            } else {
                return 3;//代表密码错误
            }
        }
        return 2;//代表用户名不存在
    }

    /*检查登录状态*/
    @RequestMapping("/checklogin")
    public boolean checkLogin(HttpSession session) {
        //取出session对象中的user对象
        User user = (User) session.getAttribute("user");
        //如果user==null说明没有登陆过返回false 反之返回true
        System.out.println(user);
        return user == null ? false : true;
    }

    /*注册新用户*/
    @RequestMapping("/register")
    public int register(User user) {
        if (user.getUsername() != null) {
            uMapper.register(user);
            /*System.out.println(user);*/
            return 1;//注册成功
        }
        return 2; //注册失败

    }

    /*退出*/
    @RequestMapping("/logout")
    public void logout(HttpSession session) {
        //把保存在session里面的用户对象删除
        session.removeAttribute("user");
    }

    /*修改个人信息*/
    @RequestMapping("/updatePersonal")
    public int updatePersonal(User user, HttpSession session) {

        User u = (User) session.getAttribute("user");
        /*通过session获取用户id和用户名username,然后进行修改信息的操作*/
        user.setId(u.getId());
        user.setUsername(u.getUsername());

        if (user.getUsername() != null) {

            uMapper.updateByUserName(user);
            /*输出测试*/
            /*System.out.println(user);*/
            System.out.println("修改信息成功");

            return 1; //1为修改成功
        } else {
            return 2; //2为修改失败
        }

    }


    @Value("${imageDirPath}")
    private String imageDirPath;

    /*上传个人头像*/
    @RequestMapping("/uploadPicture")
    public String uploadPicture(MultipartFile file, HttpSession session, User user) throws IOException {
        User u = (User) session.getAttribute("user");
        user.setId(u.getId());
        user.setUsername(u.getUsername());

        /*从前台得到图片路径保存入数据库*/
        //得到原始文件名
        String fileName = file.getOriginalFilename();
        //得到后缀名
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        //得到唯一文件名
        fileName = UUID.randomUUID() + suffix;
        //把文件保存到imageDirPath 路径下
        String path = imageDirPath + "/" + fileName;
        try {
            File fileDir = new File(path);
            System.out.println(fileDir.getAbsolutePath() + File.separator);
            file.transferTo(new File(fileDir.getAbsolutePath() + File.separator));
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*//创建User实体对象
        User user = new Banner();*/
        //把url设置给user对象
        user.setPicture("/" + fileName);
        //调用mapper的update(user)方法  把数据保存到数据库中
        uMapper.updatePicture(user);


        /*返回图片路径*/
        String url = uMapper.findPictureById(user);
       /* //得到文件的完整路径
        path = imageDirPath + url;
        user.setPicture(path);*/
        /*将路径放在session中*/
        u.setPicture(url);

        //测试上传图片功能.
        /*System.out.println("上传图片完后:" + u);

        System.out.println("user对象获取到的图片名称:" + user.getPicture());*/

        url = "/images/user" + user.getPicture();

        System.out.println(url);
        return url;

    }

    /*找图片*/
    @RequestMapping("/selectPicture")
    public String selectPicture(User user, HttpSession session) {

        User u = (User) session.getAttribute("user");
        user.setId(u.getId());


        String url = uMapper.selectPicture(user.getId());

        url = "../images/user" + String.valueOf(url);

        /*测试获取图片路径.*/
        /*System.out.println(url);
        System.out.println("查询到的图片地址为:" + uMapper.selectPicture(u.getId()));*/


        return uMapper.selectPicture(u.getId());

    }

    /*找昵称*/
    @RequestMapping("/selectNickName")
    public String selectNickName(User user, HttpSession session) {
        User u = (User) session.getAttribute("user");
        user.setId(u.getId());
        return uMapper.selectNickName(user.getId());
    }

    /*找签名*/
    @RequestMapping("/selectSign")
    public String selectSign(User user, HttpSession session) {
        User u = (User) session.getAttribute("user");
        user.setId(u.getId());
        return uMapper.selectSign(user.getId());
    }

    /*找头像,昵称,签名*/
    @RequestMapping("/selectUser")
    public List<User> selectMessage(User user, HttpSession session) {
        User u = (User) session.getAttribute("user");

        user.setId(u.getId());
        List<User> list = uMapper.selectMessage(user.getId());
        System.out.println("查到的User: " + list);
        return list;
    }

    /*改签名*/
    @RequestMapping("/updateSign")
    public int updateSign(User user, HttpSession session) {
        User u = (User) session.getAttribute("user");
        /*通过session获取用户id和用户名username,然后进行修改信息的操作*/
        user.setId(u.getId());
        user.setUsername(u.getUsername());

        if (user.getUsername() != null) {

            uMapper.updateSign(user);
            /*输出测试*/
            /*System.out.println(user);*/
            System.out.println("修改信息成功");

            return 1; //1为修改成功
        } else {
            return 2; //2为修改失败
        }
    }

    //查询用户信息
    @RequestMapping("/finduser")
    public List<User> finduser(){
        return uMapper.finduser();
    }

    //新添用户
    @RequestMapping("/createcrmclass")
    public int createcrmclass(User user){
        System.out.println(user);
        uMapper.createcrmclass(user);
        return 1;

    }
    //删除用户
    @RequestMapping("/deletecrmclass")
    public String deletecrmclass(int id){
        uMapper.deletecrmclass(id);
        return "删除成功";
    }
    //修改用户
    @RequestMapping("/updateById")
    public int updateUser(User user){
        User u = uMapper.SelectAll(user.getUsername());
        System.out.println(user.getUsername());
        if(u!=null){

            return 2;
        }
        System.out.println(user);
        uMapper.updateById(user);
        return 1;
    }

    //模糊查询
    @RequestMapping("/selectLike")
    public List<User> selectLike(String username){
        System.out.println("在这里");
        return uMapper.selectLike(username);
    }


}
