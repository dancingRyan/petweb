package com.web.petfinal.controller;

import com.web.petfinal.entity.AdoptMessage;
import com.web.petfinal.entity.Message;
import com.web.petfinal.entity.User;
import com.web.petfinal.mapper.AdoptMessageMapper;
import com.web.petfinal.mapper.MessageMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class MessageController {
    @Autowired(required = false)
    MessageMapper messageMapper;

    @Autowired(required = false)
    AdoptMessageMapper adoptMessageMapper;

    /*收集建议意见*/
    @RequestMapping("/sendMessage")
    public String sendMessage(Message message) {

        messageMapper.insertMessage(message);
        return "接收成功!";
    }

    /*增加领养宠物列表*/
    @RequestMapping("/insertAdoptMessage")
    public String insertAdoptMessage(AdoptMessage adoptMessage, HttpSession session, User user) {

        User u = (User) session.getAttribute("user");
        user.setId(u.getId());
        adoptMessage.setUserid(user.getId());

        adoptMessageMapper.insertMessage(adoptMessage);

        System.out.println(adoptMessage);

        return "接收成功!";

    }


}
