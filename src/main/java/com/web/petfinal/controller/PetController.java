package com.web.petfinal.controller;


import com.web.petfinal.entity.Blog;
import com.web.petfinal.entity.Pet;
import com.web.petfinal.entity.PetShow;
import com.web.petfinal.entity.User;
import com.web.petfinal.mapper.PetMapper;
import com.web.petfinal.mapper.PetShowMapper;
import com.web.petfinal.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
public class PetController {
    @Autowired(required = false)
    PetMapper petMapper;

    @Autowired(required = false)
    PetShowMapper petShowMapper;

    @Autowired(required = false)
    UserMapper uMapper;

    @RequestMapping("/selectpet")
    public List<PetShow> selectpet() {
        System.out.println(petShowMapper.selectShowPic());

        return petShowMapper.selectShowPic();
    }

    @Value("${imageDirPath}")
    private String imageDirPath;

    /*增加宠物文章的发布信息*/
    @RequestMapping("/insertadopt")
    public String insertadopt( HttpSession session, User user, Pet pet,MultipartFile file) throws IOException {
        /*通过session,将用户id放入pet中的user_id里,知道是哪位小帅哥/小美女 发布的文章*/

        User u = (User) session.getAttribute("user");
        user.setId(u.getId());
        pet.setUser_id(user.getId());

/*后续测试一下这个方法上传,前面没测试好*/
     /*   String picName = UUID.randomUUID().toString();
        //获取上传文件得元素得名称
        String fileName = file.getOriginalFilename();
        String substring = fileName.substring(fileName.lastIndexOf("."));
        //上传文件
        try {
            file.transferTo(new File(imageDirPath + picName + substring));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String name = picName + substring;

        return name;*/

        //调用mapper的insert(pet)方法  把数据保存到数据库中
        petMapper.insertAll(pet);

        System.out.println("输出发布宠物的信息:" + pet);
        return "增加成功!";
    }


    /*查询发布的所有宠物信息*/
    @RequestMapping("/selectPetBlog")
    public List<Pet> selectPetBlog(){
        List<Pet> list = petMapper.selectPetBlog();
        System.out.println("循环展示宠物领养信息:"+list);
        return list;
    }
    /*查询发布的所有宠物信息,并把宠物的信息放入session里*/
    @RequestMapping("/selectPetMessage")
    public List<Pet> selectPetMessage(){
        List<Pet> list = petMapper.selectPetMessage();
        System.out.println("循环展示宠物领养信息:"+list);
        return list;
    }

    /*上传存储宠物图片*/
/*    @RequestMapping("/insertpic")
    public String updatepic(MultipartFile file, HttpSession session, User user,Pet pet) throws IOException {
        User u = (User) session.getAttribute("user");
        user.setId(u.getId());

      *//*  将用户id保存在pet中的userid中,通过他,修改刚才用户上传的图片!*//*
       pet.setUser_id(user.getId());

    *//*    从前台得到图片路径保存入数据库*//*
        //得到原始文件名
        String fileName = file.getOriginalFilename();
        //得到后缀名
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        //得到唯一文件名
        fileName = UUID.randomUUID() + suffix;
        //把文件保存到imageDirPath 路径下
        String path = imageDirPath + "/" + fileName;
        try {
            File fileDir = new File(path);
            System.out.println(fileDir.getAbsolutePath() + File.separator);
            file.transferTo(new File(fileDir.getAbsolutePath() + File.separator));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //把url设置给user对象
        pet.setPic("/" + pet.getPic());
        //调用mapper的update(user)方法  把数据保存到数据库中
        petMapper.updatepic(pet);

        System.out.println("宠物信息:"+pet);


        return "收到!";

    }*/




}
