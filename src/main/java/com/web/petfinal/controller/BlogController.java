package com.web.petfinal.controller;


import com.web.petfinal.entity.Banner;
import com.web.petfinal.entity.Blog;
import com.web.petfinal.mapper.BannerMapper;
import com.web.petfinal.mapper.BlogMapper;
import com.web.petfinal.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BlogController {
    @Autowired(required = false)
    BlogMapper bMapper;

    @Autowired(required = false)
    UserMapper uMapper;
/*找团队博客*/
    @RequestMapping("/selectBlog")
    public List<Blog> selectbanner(){
        List<Blog> list = bMapper.selectBlog();
        System.out.println(list);
        return list;
    }

}
