package com.web.petfinal.controller;


import com.web.petfinal.entity.Admin;
import com.web.petfinal.entity.User;
import com.web.petfinal.mapper.AdminMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
public class AdminController {
    @Autowired(required = false)
    AdminMapper adminMapper;

    /*查团队博客*/
    @RequestMapping("/selectTeamShow")
    public List<Admin> selectTeamShow() {
        System.out.println(adminMapper.selectTeamShow());
        return adminMapper.selectTeamShow();
    }

    /*登录判断*/
    @RequestMapping("/adminlogin")
    public int login(Admin admin, String rem, HttpServletResponse response,
                     HttpSession adminsession) {
        //查询管理员输入的用户名对应的数据库中的信息
        Admin a = adminMapper.adminlogin(admin.getUsername());

        if (a != null) {//查询到用户名对应的信息
            if (admin.getPassword().equals(a.getPassword())) {
                //把登录成功的user对象装进session里面

                a = adminMapper.SelectAll(admin.getUsername());

                adminsession.setAttribute("admin", a);
                /*判断是否获取到id了*/
                System.out.println("admin =" + a + ",rem=" + rem);

                //判断是否需要记住用户名和密码
                if (rem != null) {
                    //创建Cookie对象 把用户名和密码保存进去
                    Cookie c1 = new Cookie("username", admin.getUsername());
                    Cookie c2 = new Cookie("password", admin.getPassword());
                    //设置Cookie的保存时间 单位秒
                    c1.setMaxAge(60 * 60 * 24 * 30);
                    //通过响应对象response把cookie下发给客户端
                    response.addCookie(c1);
                    response.addCookie(c2);
                }
                return 1;//代表登录成功
            } else {
                return 3;//代表密码错误
            }
        }
        return 2;//代表用户名不存在
    }
    /*退出*/
    @RequestMapping("/adminlogout")
    public void adminlogout(HttpSession adminsession) {
        //把保存在session里面的用户对象删除
        adminsession.removeAttribute("admin");
    }

    /*检查登录状态*/
    @RequestMapping("/checkadminlogin")
    public boolean checkLogin(HttpSession session) {
        //取出session对象中的user对象
        Admin admin = (Admin) session.getAttribute("admin");
        //如果user==null说明没有登陆过返回false 反之返回true
        System.out.println(admin);
        return admin == null ? false : true;
    }


    /*管理员信息获取*/
    @RequestMapping("/findadmin")
    public List<Admin> findadmin(){
        return adminMapper.findadmin();
    }

    //新添管理员
    @RequestMapping("/createadmin")
    public int createcrmadmin(Admin admin){
        adminMapper.createcrmadmin(admin);
        return 1;

    }
    //删除管理员
    @RequestMapping("/deleteadmin")
    public String deleteadmin(int id){
        adminMapper.deleteadmin(id);
        return "删除成功";
    }
    //修改管理员
    @RequestMapping("/updateadminById")
    public int updateAdmin(Admin admin){
        Admin a = adminMapper.SelectAll(admin.getUsername());
        System.out.println(admin.getUsername());
        if(a!=null){
            return 2;
        }
        adminMapper.updateById(admin);
        return 1;
    }

    //模糊查询
    @RequestMapping("/selectLikeadmin")
    public List<Admin> selectLike(String username){
        System.out.println("在这里");
        return adminMapper.selectLike(username);
    }



}
