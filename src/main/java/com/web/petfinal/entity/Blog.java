package com.web.petfinal.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import java.sql.Date;

@Data
@ToString
@EqualsAndHashCode
/*团队日志博客表*/
public class Blog {

    private Integer id;
    private Date actionTime;
    private String address;
    private String peoples;
    private String event;
    private String title;
    private String picture;


}
