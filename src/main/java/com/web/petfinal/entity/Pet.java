package com.web.petfinal.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@EqualsAndHashCode
/*宠物信息表*/
public class Pet {

    private Integer id;
    private String title;
    private String petname;
    private String petType;
    private String sex;
    private String birthday;
    private String pic;
    private Integer state;
    private String remark;
    private Integer user_id;




}
