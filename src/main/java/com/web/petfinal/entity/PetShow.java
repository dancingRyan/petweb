package com.web.petfinal.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@EqualsAndHashCode
/*萌宠展示表*/
public class PetShow {

    private Integer id;
    private String petname;
    private String showpic;


}
