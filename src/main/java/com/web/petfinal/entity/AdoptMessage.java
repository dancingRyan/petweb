package com.web.petfinal.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
/*用户领养申请信息表*/
public class AdoptMessage {
    private Integer id;
    private String name;
    private String sex;
    private String telephone;
    private String email;
    private String address;
    private String remark;
    private Integer userid;
    private Integer petid;
}
