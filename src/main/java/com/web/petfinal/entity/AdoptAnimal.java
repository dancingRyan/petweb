package com.web.petfinal.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@EqualsAndHashCode
/*宠物领养状态查询表*/
public class AdoptAnimal {
    private Integer id;
    private Integer userId;
    private Integer petId;
    private String adoptTime;
    private Integer state;
    private Integer request;
    private String applyTime;

}
