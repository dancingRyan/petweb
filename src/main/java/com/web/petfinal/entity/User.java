package com.web.petfinal.entity;


import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.sql.Date;


@Data
@ToString
@EqualsAndHashCode
/*用户信息表*/
public class User {
//    @TableId
    private Integer id;

    private String username;
    private String password;
    private String nickname;
    private String sex;
    private Integer age;
    private String telephone;
    private Date birthday;
    private String Email;
    private String address;
    private String picture;
    private Integer state;
    private String sign;
    private Integer request;
    private Date applyTime;


}
