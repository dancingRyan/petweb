package com.web.petfinal.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.sql.Date;


@Data
@ToString
@EqualsAndHashCode
/*管理员信息表*/
public class Admin {
//    @TableId
    private Integer id;

    private String username;
    private String password;
    private String nickname;
    private String sex;
    private Integer age;
    private String telephone;
    private Date birthday;
    private String Email;
    private String address;
    private String picture;
    private String sign;


}
