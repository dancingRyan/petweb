package com.web.petfinal.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@EqualsAndHashCode
/*留言反馈表*/
public class Message {

    private Integer id;
    private String name;
    private String email;
    private String subject;
    private String tel;
    private String message;


}
