package com.web.petfinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetfinalApplication {

    public static void main(String[] args) {
        SpringApplication.run(PetfinalApplication.class, args);
    }

}
